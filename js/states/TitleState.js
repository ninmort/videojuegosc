var Formularium = Formularium || {};

Formularium.TitleState = function () {
    "use strict";
    Formularium.JSONLevelState.call(this);

    this.prefab_classes = {
        "text": Formularium.TextPrefab.prototype.constructor,
        "login_button": Formularium.LoginButton.prototype.constructor,
        "imagen" : Formularium.Imagen.prototype.constructor
    };
};

Formularium.TitleState.prototype = Object.create(Formularium.JSONLevelState.prototype);
Formularium.TitleState.prototype.constructor = Formularium.TitleState;

Formularium.TitleState.prototype.create = function () {
    "use strict";
    var formularium_data;
    Formularium.JSONLevelState.prototype.create.call(this);
};

Formularium.TitleState.prototype.on_login = function (result) {
    "use strict";
    firebase.database().ref("/users/" + result.user.uid).once("value").then(this.start_game.bind(this));
};

Formularium.TitleState.prototype.start_game = function (snapshot) {
    "use strict";
    var user_data;
    user_data = snapshot.val();
    // if (!user_data) {
    //     this.game.caught_formularium = [];
    //     this.game.number_of_pokeballs = {pokeball: 0, greatball: 1, ultraball: 2};
    // } else {
    //     this.game.caught_formularium = user_data.caught_formularium || [];
    //     this.game.number_of_pokeballs = user_data.number_of_pokeballs || {pokeball: 0, greatball: 1, ultraball: 2};
    // }

    if (!user_data) {
        this.game.level = 0;
        this.game.starsArray = [0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4];

    } else {
        this.game.level = user_data.level || 0;
        this.game.starsArray = user_data.starsArray || [0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4];
    }

    game.global = {
        thumbRows : 5,
        // number of thumbnail cololumns
        thumbCols : 4,
        // width of a thumbnail, in pixels
        thumbWidth : 64,
        // height of a thumbnail, in pixels
        thumbHeight : 64,
        // space among thumbnails, in pixels
        thumbSpacing : 8,
        // array with finished levels and stars collected.
        // 0 = playable yet unfinished level
        // 1, 2, 3 = level finished with 1, 2, 3 stars
        // 4 = locked
        //starsArray : [0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
        // level currently playing
        //level : 0
    }
    game.global.starsArray = this.game.starsArray;
    game.global.level = this.game.level;

    this.game.state.start("BootState", true, false, "assets/levels/level_select.json", "LevelSelect");
    // this.game.state.start("BootState", true, false, "assets/levels/world_level.json", "WorldState");
};
