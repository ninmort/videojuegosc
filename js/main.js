var Formularium = Formularium || {};

var config = {
    apiKey: "AIzaSyAycQshGLDieSpyH-pXrkjBkCJ6wQ-E0-0",
    authDomain: "videojuegos-sc-9edd1.firebaseapp.com",
    databaseURL: "https://videojuegos-sc-9edd1.firebaseio.com",
    storageBucket: "videojuegos-sc-9edd1.appspot.com",
    messagingSenderId: "234942242767"
  };
firebase.initializeApp(config);

var game = new Phaser.Game(1280, 960, Phaser.CANVAS);

game.state.add("BootState", new Formularium.BootState());
game.state.add("LoadingState", new Formularium.LoadingState());
game.state.add("TitleState", new Formularium.TitleState());
game.state.add("LevelSelect", new Formularium.LevelSelect());
game.state.add("PlayLevel", new Formularium.PlayLevel());
game.state.start("BootState", true, false, "assets/levels/title_screen.json", "TitleState");
